import React from "react";
import { Button, Card } from "react-bootstrap";

export default function CardComponent({item}) {
    console.log("Item : ", item);
  return (
    <div>
      <Card>
        <Card.Img variant="top" src={item.image} />
        <Card.Body>
          <Card.Title>{item.title}</Card.Title>
          <Card.Text>
            {item.description}
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </div>
  );
}
