import logo from "./logo.svg";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import HomeComponent from "./pages/HomeComponent";
import { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [data, setData] = useState([])
  // useEffect(() => {
  //   fetch("http://110.74.194.124:3034/api/articles")
  //     .then((response) => response.json())
  //     .then((res) => setData(res.data));
  // }, []);

useEffect(() => {
  axios.get('http://110.74.194.124:3034/api/articles').then((res) => setData(res.data.data));
}, [])

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomeComponent data={data}/>} />
      </Routes>
    </div>
  );
}

export default App;
