import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import CardComponent from "../components/CardComponent";

export default function HomeComponent({ data }) {
  console.log("Home : ", data);
  return (
    <div>
      <Container>
        <Row>{data.map((item, idx) => (
            <Col key={idx} xs={12} sm={6} md={4}>
                <CardComponent item={item}/>
            </Col>
        ))}</Row>
      </Container>
    </div>
  );
}
